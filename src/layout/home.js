import { useContext, useEffect, useState } from "react";

import MuiAppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import MuiDrawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Toolbar from "@mui/material/Toolbar";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";

import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import CloudDownloadIcon from "@mui/icons-material/CloudDownload";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import LogoutIcon from "@mui/icons-material/Logout";
import MenuIcon from "@mui/icons-material/Menu";
import UploadFileIcon from "@mui/icons-material/UploadFile";

import { styled, useTheme } from "@mui/material/styles";

import LandingPage from "../pages";

import Download from "../components/download";
import FileUpload from "../components/file-upload";
import Filtering from "../components/filtering/filtering";

import AuthContext from "../context/AuthProvider";
import useAuth from "../hooks/useAuth";
import useAxiosPrivate from "../hooks/useAxiosPrivate";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

const Home = () => {
  const theme = useTheme();
  const axiosPrivate = useAxiosPrivate();
  const { setAuth } = useContext(AuthContext);
  const { auth } = useAuth();

  const [referenceGenomes, setReferenceGenomes] = useState([]);
  const [files, setFiles] = useState([]);
  const [sessions, setSessions] = useState([]);
  const [variantFiles, setVariantFiles] = useState([]);

  const [open, setOpen] = useState(false);
  const [isFileUploadDialogOpen, setIsFileUploadDialogOpen] = useState(false);

  const [isFilteringDialogOpen, setIsFilteringDialogOpen] = useState(false);
  const [allFiles, setAllFiles] = useState([]);

  const [isDownloadDialogOpen, setIsDownloadDialogOpen] = useState(false);

  useEffect(() => {
    Promise.all([
      axiosPrivate.get("/reference"),
      axiosPrivate.get("/file"),
      axiosPrivate.get("/session"),
    ]).then(
      ([
        { data: refGenomsResponse },
        { data: filesResponse },
        { data: sessionResponse },
      ]) => {
        setReferenceGenomes(refGenomsResponse);
        setFiles(filesResponse);
        setSessions(sessionResponse);
        setVariantFiles(
          filesResponse.filter((file) => file.type === "variant")
        );
        setAllFiles([...refGenomsResponse, ...filesResponse]);
      }
    );
  }, [axiosPrivate]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleFileUploadOpen = () => {
    setIsFileUploadDialogOpen(true);
  };

  const handleFileUploadClose = (doRefresh, fileTypeId) => {
    if (doRefresh) {
      refreshReferenceGenomesAndFiles();
    }
    setIsFileUploadDialogOpen(false);
  };

  const handleFilteringOpen = () => {
    setIsFilteringDialogOpen(true);
  };

  const handleFilteringClose = (doRefresh, fileTypeId) => {
    if (doRefresh) {
      refreshReferenceGenomesAndFiles();
    }
    setIsFilteringDialogOpen(false);
  };

  const handleDownloadOpen = () => {
    setIsDownloadDialogOpen(true);
  };

  const handleDownloadClose = () => {
    setIsDownloadDialogOpen(false);
  };

  const handleLogout = () => {
    setAuth({});
  };

  const refreshReferenceGenomesAndFiles = () => {
    Promise.all([
      axiosPrivate.get("/reference"),
      axiosPrivate.get("/file"),
    ]).then(([{ data: refGenomsResponse }, { data: filesResponse }]) => {
      setReferenceGenomes(refGenomsResponse);
      setFiles(filesResponse);
      setVariantFiles(filesResponse.filter((file) => file.type === "variant"));
      setAllFiles([...refGenomsResponse, ...filesResponse]);
    });
  };

  const refreshSources = (refrenceGenomes, files) => {
    setReferenceGenomes(refrenceGenomes);
    setFiles(files);
    setVariantFiles(files.filter((file) => file.type === "variant"));
    setAllFiles([...refrenceGenomes, ...files]);
  };

  return (
    <>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar position="fixed" open={open}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              sx={{
                marginRight: 5,
                ...(open && { display: "none" }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap component="div">
              Bioinformatic data visualization and manipulation
            </Typography>
            <Typography sx={{ marginLeft: "auto", marginRight: 2 }}>
              {auth?.email}
            </Typography>
            <Tooltip title="Logut">
              <IconButton onClick={handleLogout} edge="end">
                <LogoutIcon />
              </IconButton>
            </Tooltip>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <DrawerHeader>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "rtl" ? (
                <ChevronRightIcon />
              ) : (
                <ChevronLeftIcon />
              )}
            </IconButton>
          </DrawerHeader>
          <Divider />
          <List>
            <ListItem disablePadding sx={{ display: "block" }}>
              <ListItemButton
                sx={{
                  minHeight: 48,
                  justifyContent: open ? "initial" : "center",
                  px: 2.5,
                }}
                onClick={handleFileUploadOpen}
              >
                <Tooltip title="Upload files">
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    <UploadFileIcon />
                  </ListItemIcon>
                </Tooltip>
                <ListItemText
                  primary="File upload"
                  sx={{ opacity: open ? 1 : 0 }}
                />
              </ListItemButton>
            </ListItem>
            <ListItem disablePadding sx={{ display: "block" }}>
              <ListItemButton
                sx={{
                  minHeight: 48,
                  justifyContent: open ? "initial" : "center",
                  px: 2.5,
                }}
                onClick={handleFilteringOpen}
              >
                <Tooltip title="Filter files">
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    <FilterAltIcon />
                  </ListItemIcon>
                </Tooltip>
                <ListItemText
                  primary="Filtering"
                  sx={{ opacity: open ? 1 : 0 }}
                />
              </ListItemButton>
            </ListItem>
            <ListItem disablePadding sx={{ display: "block" }}>
              <ListItemButton
                sx={{
                  minHeight: 48,
                  justifyContent: open ? "initial" : "center",
                  px: 2.5,
                }}
                onClick={handleDownloadOpen}
              >
                <Tooltip title="Download">
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: open ? 3 : "auto",
                      justifyContent: "center",
                    }}
                  >
                    <CloudDownloadIcon />
                  </ListItemIcon>
                </Tooltip>
                <ListItemText
                  primary="Download"
                  sx={{ opacity: open ? 1 : 0 }}
                />
              </ListItemButton>
            </ListItem>
          </List>
        </Drawer>
        <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
          <DrawerHeader />
          <LandingPage
            referenceGenomes={referenceGenomes}
            files={files}
            sessions={sessions}
            variantFiles={variantFiles}
            setReferenceGenomes={setReferenceGenomes}
            setFiles={setFiles}
            setSessions={setSessions}
            refreshSources={refreshSources}
          />
          <div id="igv-div"></div>
        </Box>
      </Box>
      <FileUpload
        open={isFileUploadDialogOpen}
        onClose={handleFileUploadClose}
      />
      <Filtering
        open={isFilteringDialogOpen}
        onClose={handleFilteringClose}
        files={allFiles}
      />
      <Download
        open={isDownloadDialogOpen}
        onClose={handleDownloadClose}
        files={allFiles}
      />
    </>
  );
};

export default Home;
