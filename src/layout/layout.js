import { Outlet } from "react-router-dom";

const LayoutNew = () => {
  return (
    <>
      <Outlet />
    </>
  );
};

export default LayoutNew;
