import { Route, Routes } from "react-router-dom";

import Home from "./layout/home";
import LayoutNew from "./layout/layout";

import Login from "./components/login";
import Register from "./components/register";
import RequireAuth from "./components/require-auth";

function App() {
  return (
    <Routes>
      <Route path="/" element={<LayoutNew />}>
        {/* Public routes */}
        <Route path="login" element={<Login />} />
        <Route path="register" element={<Register />} />

        {/* Protected routes */}
        <Route element={<RequireAuth />}>
          <Route path="/" element={<Home />} />
        </Route>
      </Route>
    </Routes>
  );
}

export default App;
