import { useEffect } from "react";

import igv from "igv/dist/igv";
import useAuth from "../../hooks/useAuth";

import { BASE_URL } from "../../services/constants";

const Igv = (props) => {
  const { id, name } = props.referenceGenomeData;
  const { auth } = useAuth();

  useEffect(() => {
    var igvDiv = document.getElementById("igv-div");
    var options = {
      reference: {
        id: name,
        name: name,
        fastaURL: `${BASE_URL}/reference/${id}`,
        indexURL: `${BASE_URL}/reference/index/${id}`,
        headers: { Authorization: `Bearer ${auth?.accessToken}` },
      },
    };

    igv.createBrowser(igvDiv, options).then(function (browser) {
      igv.browser = browser;
    });

    return () => {
      igv.removeAllBrowsers();
    };
  }, [auth?.accessToken, id, name]);
};

export default Igv;
