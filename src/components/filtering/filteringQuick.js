import { useEffect, useState } from "react";

import { LoadingButton } from "@mui/lab";
import {
  Box,
  Checkbox,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
} from "@mui/material";

import { axiosPrivate } from "../../services/axios";

import { stringifyNames } from "./util";

import { useFormik } from "formik";
import { enqueueSnackbar } from "notistack";
import * as yup from "yup";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const FilteringQuick = (props) => {
  const { onClose, open, files } = props;

  const [loading, setLoading] = useState(false);
  const [selectedFiles, setSelectedFiles] = useState([]);

  const validation = yup.object({
    fileIds: yup.array().min(1, "Select one file"),
    fileName: yup.string().required("File name is required"),
    sequence: yup.string().required("Sequence is required"),
    startPosition: yup.number().required("Start position is required"),
    endPosition: yup.number().required("End position is required"),
  });

  const formik = useFormik({
    initialValues: {
      fileIds: files.map((file) => file.name),
      fileName: "",
      sequence: "",
      filterBySequence: false,
      startPosition: 0,
      endPosition: 0,
    },
    validationSchema: validation,
    onSubmit: (values) => {
      filter(values);
    },
    enableReinitialize: true,
  });

  useEffect(() => {
    if (files.length > 0) {
      const preselectedFiles = files.map((file) => file.name);
      const newValues = stringifyNames(preselectedFiles);

      setSelectedFiles(newValues);
    }
  }, [files]);

  const handleClose = (doRefresh) => {
    onClose(doRefresh);
    setSelectedFiles([]);
    formik.resetForm();
  };

  const filter = (values) => {
    setLoading(true);

    const selectedFiles = values.fileIds.map(
      (fileId) => files.filter((file) => file.name === fileId)[0]
    );

    selectedFiles.forEach(async (file, index) => {
      const value = {
        fileId: file.id,
        fileName: values.fileName,
        sequence: values.sequence,
        filterBySequence: values.filterBySequence,
        startPosition: values.startPosition,
        endPosition: values.endPosition,
      };

      if (file.hasOwnProperty("type")) {
        await axiosPrivate.post("/processing/file-filter", value);
      } else {
        await axiosPrivate.post("/processing/reference-filter", value);
      }

      if (selectedFiles.length === index + 1) {
        enqueueSnackbar("Finished filtering");
        handleClose(true);
        setLoading(false);
      }
    });
  };

  const handleChange = (event) => {
    const newValue = stringifyNames(event.target.value);

    setSelectedFiles(newValue);
    formik.setFieldValue("fileIds", newValue);
  };

  const handleFileRemove = (fileName) => {
    const removedFileName = selectedFiles.filter((file) => file !== fileName);
    const newValue = stringifyNames(removedFileName);

    setSelectedFiles(newValue);
    formik.setFieldValue("fileIds", newValue);
  };

  return (
    <Dialog fullWidth size="sm" open={open} onClose={() => handleClose(false)}>
      <DialogTitle>File filtering</DialogTitle>
      <Box
        sx={{ padding: 1 }}
        component="form"
        onSubmit={formik.handleSubmit}
        onReset={formik.handleReset}
      >
        <DialogContent>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <FormControl
                fullWidth
                error={formik.touched.fileIds && Boolean(formik.errors.fileIds)}
              >
                <InputLabel id="files-label">Files</InputLabel>
                <Select
                  labelId="files-label"
                  id="fileIds"
                  name="fileIds"
                  multiple
                  value={formik.values.fileIds}
                  onChange={handleChange}
                  input={<OutlinedInput label="Files" />}
                  renderValue={(selected) => (
                    <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                      {selected.map((value) => (
                        <Chip
                          key={value}
                          label={value}
                          onDelete={() => handleFileRemove(value)}
                          onMouseDown={(event) => {
                            event.stopPropagation();
                          }}
                        />
                      ))}
                    </Box>
                  )}
                  MenuProps={MenuProps}
                >
                  {files.map((file) => (
                    <MenuItem key={file.name} value={file.name}>
                      <Checkbox
                        checked={selectedFiles.indexOf(file.name) > -1}
                      />
                      <ListItemText primary={file.name} />
                    </MenuItem>
                  ))}
                </Select>
                {formik.touched.fileIds && Boolean(formik.errors.fileIds) && (
                  <FormHelperText>{formik.errors.fileIds}</FormHelperText>
                )}
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                id="fileName"
                name="fileName"
                label="File name"
                variant="outlined"
                value={formik.values.fileName}
                onChange={formik.handleChange}
                error={
                  formik.touched.fileName && Boolean(formik.errors.fileName)
                }
                helperText={formik.touched.fileName && formik.errors.fileName}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                select
                id="sequence"
                name="sequence"
                value={formik.values.sequence}
                label="Sequence"
                onChange={formik.handleChange}
                error={
                  formik.touched.sequence && Boolean(formik.errors.sequence)
                }
                helperText={formik.touched.sequence && formik.errors.sequence}
              >
                {files.length > 0
                  ? files[0].sequences.map((file) => (
                      <MenuItem key={file} value={file}>
                        {file}
                      </MenuItem>
                    ))
                  : []}
              </TextField>
            </Grid>
            <Grid item xs={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    id="filterBySequence"
                    name="filterBySequence"
                    value={formik.values.filterBySequence}
                    onChange={formik.handleChange}
                  />
                }
                label="Filter only by sequence"
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                id="startPosition"
                name="startPosition"
                label="Start position"
                variant="outlined"
                type="number"
                disabled={formik.values.filterBySequence}
                value={formik.values.startPosition}
                onChange={formik.handleChange}
                error={
                  formik.touched.startPosition &&
                  Boolean(formik.errors.startPosition)
                }
                helperText={
                  formik.touched.startPosition && formik.errors.startPosition
                }
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                id="endPosition"
                name="endPosition"
                label="End position"
                variant="outlined"
                type="number"
                disabled={formik.values.filterBySequence}
                value={formik.values.endPosition}
                onChange={formik.handleChange}
                error={
                  formik.touched.endPosition &&
                  Boolean(formik.errors.endPosition)
                }
                helperText={
                  formik.touched.endPosition && formik.errors.endPosition
                }
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <LoadingButton loading={loading} variant="contained" type="submit">
            Filter file
          </LoadingButton>
        </DialogActions>
      </Box>
    </Dialog>
  );
};

export default FilteringQuick;
