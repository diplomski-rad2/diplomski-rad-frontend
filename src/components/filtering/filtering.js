import { useState } from "react";

import { LoadingButton } from "@mui/lab";
import {
  Box,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Grid,
  ListSubheader,
  MenuItem,
  TextField,
} from "@mui/material";

import { axiosPrivate } from "../../services/axios";

import { useFormik } from "formik";
import { enqueueSnackbar } from "notistack";
import * as yup from "yup";

import {
  ALIGNMENT,
  ANNOTATION,
  FileType,
  VARIANT,
} from "../../services/constants";

const Filtering = (props) => {
  const { onClose, open, files } = props;

  const [loading, setLoading] = useState(false);

  const validation = yup.object({
    fileId: yup.string().required("File is required"),
    fileName: yup.string().required("File name is required"),
    sequence: yup.string().required("Sequence is required"),
    startPosition: yup.number().required("Start position is required"),
    endPosition: yup.number().required("End position is required"),
  });

  const formik = useFormik({
    initialValues: {
      fileId: "",
      fileName: "",
      sequence: "",
      filterBySequence: false,
      startPosition: 0,
      endPosition: 0,
    },
    validationSchema: validation,
    onSubmit: (values) => {
      filter(values);
    },
    enableReinitialize: true,
  });

  const handleClose = (doRefresh, fileTypeId) => {
    onClose(doRefresh, fileTypeId);
    formik.resetForm();
  };

  const filter = (values) => {
    setLoading(true);
    const selectedFile = files.filter((file) => file.id === values.fileId)[0];
    let fileTypeId = FileType.REFERENCE_GENOME;
    let endopointCall = "";

    if (selectedFile.hasOwnProperty("type")) {
      endopointCall = axiosPrivate.post("/processing/file-filter", values);
      fileTypeId = FileType.FILE;
    } else {
      endopointCall = axiosPrivate.post("/processing/reference-filter", values);
    }

    endopointCall
      .then(() => {
        enqueueSnackbar("File filtered!");
        handleClose(true, fileTypeId);
      })
      .finally(() => setLoading(false));
  };

  return (
    <Dialog onClose={() => handleClose(false)} open={open} size="sm" fullWidth>
      <DialogTitle>File filtering</DialogTitle>
      <Box
        sx={{ padding: 1 }}
        component="form"
        onSubmit={formik.handleSubmit}
        onReset={formik.handleReset}
      >
        <DialogContent>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                select
                id="fileId"
                name="fileId"
                value={formik.values.fileId}
                label="File"
                onChange={formik.handleChange}
                error={formik.touched.fileId && Boolean(formik.errors.fileId)}
                helperText={formik.touched.fileId && formik.errors.fileId}
              >
                <ListSubheader>{ALIGNMENT.toUpperCase()}</ListSubheader>
                {files
                  .filter((file) => file.type === ALIGNMENT)
                  .map((file) => (
                    <MenuItem key={file.id} value={file.id}>
                      {file.name}
                    </MenuItem>
                  ))}

                <ListSubheader>{VARIANT.toUpperCase()}</ListSubheader>
                {files
                  .filter((file) => file.type === VARIANT)
                  .map((file) => (
                    <MenuItem key={file.id} value={file.id}>
                      {file.name}
                    </MenuItem>
                  ))}

                <ListSubheader>{ANNOTATION.toUpperCase()}</ListSubheader>
                {files
                  .filter((file) => file.type === ANNOTATION)
                  .map((file) => (
                    <MenuItem key={file.id} value={file.id}>
                      {file.name}
                    </MenuItem>
                  ))}
              </TextField>
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                id="fileName"
                name="fileName"
                label="File name"
                variant="outlined"
                value={formik.values.fileName}
                onChange={formik.handleChange}
                error={
                  formik.touched.fileName && Boolean(formik.errors.fileName)
                }
                helperText={formik.touched.fileName && formik.errors.fileName}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                select
                id="sequence"
                name="sequence"
                value={formik.values.sequence}
                label="Sequence"
                onChange={formik.handleChange}
                error={
                  formik.touched.sequence && Boolean(formik.errors.sequence)
                }
                helperText={formik.touched.sequence && formik.errors.sequence}
              >
                {formik.values.fileId !== ""
                  ? files
                      .filter((file) => file.id === formik.values.fileId)[0]
                      .sequences.map((file) => (
                        <MenuItem key={file} value={file}>
                          {file}
                        </MenuItem>
                      ))
                  : []}
              </TextField>
            </Grid>
            <Grid item xs={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    id="filterBySequence"
                    name="filterBySequence"
                    value={formik.values.filterBySequence}
                    onChange={formik.handleChange}
                  />
                }
                label="Filter only by sequence"
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                id="startPosition"
                name="startPosition"
                label="Start position"
                variant="outlined"
                type="number"
                disabled={formik.values.filterBySequence}
                value={formik.values.startPosition}
                onChange={formik.handleChange}
                error={
                  formik.touched.startPosition &&
                  Boolean(formik.errors.startPosition)
                }
                helperText={
                  formik.touched.startPosition && formik.errors.startPosition
                }
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                id="endPosition"
                name="endPosition"
                label="End position"
                variant="outlined"
                type="number"
                disabled={formik.values.filterBySequence}
                value={formik.values.endPosition}
                onChange={formik.handleChange}
                error={
                  formik.touched.endPosition &&
                  Boolean(formik.errors.endPosition)
                }
                helperText={
                  formik.touched.endPosition && formik.errors.endPosition
                }
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <LoadingButton loading={loading} variant="contained" type="submit">
            Filter file
          </LoadingButton>
        </DialogActions>
      </Box>
    </Dialog>
  );
};

export default Filtering;
