import { useState } from 'react';

import DownloadIcon from '@mui/icons-material/Download';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, List, ListItem, ListItemText, TextField } from "@mui/material";

import { fileDownload, referenceGenomeDownload } from '../../services/utils';

const Download = (props) => {
    const { onClose, open, files } = props;

    const [fileName, setFileName] = useState('');

    const handleClose = () => {
        onClose();
    };

    const handleDownload = (file) => {
        if (file.hasOwnProperty('type')) {
            fileDownload(file.id);
        } else {
            referenceGenomeDownload(file.id);
        }
    }

    return <Dialog onClose={handleClose} open={open} size='sm' fullWidth>
        <DialogTitle>Download</DialogTitle>
        <DialogContent>
            <TextField
                fullWidth
                id="search"
                label="File name"
                value={fileName}
                onChange={(event) => {
                    setFileName(event.target.value);
                }}
                sx={{ mt: 1 }}
            />
            <Box sx={{ height: 300, overflowY: 'auto' }} mt={1}>
                <List >
                    {files.filter(file => file.name.toLowerCase().includes(fileName.toLowerCase())).map(file => (
                        <ListItem
                            key={file.id}
                            secondaryAction={
                                <IconButton edge="end" aria-label="download" onClick={() => handleDownload(file)}>
                                    <DownloadIcon />
                                </IconButton>
                            }
                        >
                            <ListItemText
                                primary={file.name}
                            />
                        </ListItem>))}
                </List>
            </Box>
        </DialogContent>
        <DialogActions>
            <Button variant='contained' onClick={handleClose}>
                Close
            </Button>
        </DialogActions>
    </Dialog>
}

export default Download;