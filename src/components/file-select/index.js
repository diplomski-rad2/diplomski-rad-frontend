import { useState } from "react";

import DownloadIcon from "@mui/icons-material/Download";
import {
  FormControl,
  InputLabel,
  ListItemIcon,
  ListItemText,
  ListSubheader,
  MenuItem,
  Select,
} from "@mui/material";

import { ALIGNMENT, ANNOTATION, VARIANT } from "../../services/constants";
import {
  fileDownload,
  loadAlignmentTrack,
  loadAnnotationTrack,
  loadVariantTrack,
} from "../../services/utils";
import useAuth from "../../hooks/useAuth";

const FileSelect = (props) => {
  const { auth } = useAuth();

  const { files, disabled } = props;

  const [selectedFile, setSelectedFile] = useState("");

  const handleChange = (event) => {
    const fileId = event.target.value;
    const file = files.filter((file) => file.id === fileId)[0];

    setSelectedFile(fileId);
    loadTrackBasedOnFileType(file);
  };

  const loadTrackBasedOnFileType = (file) => {
    switch (file.type) {
      case ALIGNMENT:
        return loadAlignmentTrack(file.name, file.id, auth?.accessToken);
      case VARIANT:
        return loadVariantTrack(file.name, file.id, auth?.accessToken);
      case ANNOTATION:
        return loadAnnotationTrack(file.name, file.id, auth?.accessToken);
      default:
        return;
    }
  };

  return (
    <FormControl fullWidth>
      <InputLabel id="files-select-label">Files</InputLabel>
      <Select
        sx={{ textAlign: "left" }}
        labelId="files-select-label"
        id="files-select"
        value={selectedFile}
        label="Files"
        onChange={handleChange}
        renderValue={(selected) =>
          files.filter((file) => file.id === selected)[0].name
        }
        disabled={disabled}
      >
        <ListSubheader>{ALIGNMENT.toUpperCase()}</ListSubheader>
        {files
          .filter((file) => file.type === ALIGNMENT)
          .map((file) => (
            <MenuItem key={file.id} value={file.id}>
              <ListItemText primary={file.name} />
              <ListItemIcon
                onClick={(event) => {
                  event.stopPropagation();
                  fileDownload(file.id);
                }}
              >
                <DownloadIcon />
              </ListItemIcon>
            </MenuItem>
          ))}

        <ListSubheader>{VARIANT.toUpperCase()}</ListSubheader>
        {files
          .filter((file) => file.type === VARIANT)
          .map((file) => (
            <MenuItem key={file.id} value={file.id}>
              <ListItemText primary={file.name} />
              <ListItemIcon
                onClick={(event) => {
                  event.stopPropagation();
                  fileDownload(file.id);
                }}
              >
                <DownloadIcon />
              </ListItemIcon>
            </MenuItem>
          ))}

        <ListSubheader>{ANNOTATION.toUpperCase()}</ListSubheader>
        {files
          .filter((file) => file.type === ANNOTATION)
          .map((file) => (
            <MenuItem key={file.id} value={file.id}>
              <ListItemText primary={file.name} />
              <ListItemIcon
                onClick={(event) => {
                  event.stopPropagation();
                  fileDownload(file.id);
                }}
              >
                <DownloadIcon />
              </ListItemIcon>
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};

export default FileSelect;
