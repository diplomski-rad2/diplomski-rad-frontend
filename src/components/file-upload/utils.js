import { ALIGNMENT_FILE_EXTENSIONS, ANNOTATION_FILE_EXTENSIONS, FILE_TYPES, REFERENCE_FILE_EXTENSIONS, VARIANT_FILE_EXTENSIONS } from "./constants";

export const getFileDetails = (fileName) => {
    const lastDot = fileName.lastIndexOf('.');

    const name = fileName.substring(0, lastDot);
    const extension = fileName.substring(lastDot + 1);

    return { name, extension };
}

export const getAllowedFileExtensions = (fileTypeId) => {
    const fileType = FILE_TYPES.find(fileType => fileType.id === fileTypeId);

    switch (fileType.id) {
        case 1:
            return REFERENCE_FILE_EXTENSIONS;
        case 2:
            return ALIGNMENT_FILE_EXTENSIONS;
        case 3:
            return ANNOTATION_FILE_EXTENSIONS;
        case 4:
            return VARIANT_FILE_EXTENSIONS;
        default:
            return [];
    }
}