import React, { useState } from "react";

import { LoadingButton } from "@mui/lab";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import { Box } from "@mui/system";

import { getAllowedFileExtensions, getFileDetails } from "./utils";

import { axiosPrivate } from "../../services/axios";

import { FileType } from "../../services/constants";
import { FILE_TYPES, REFERENCE_FILE_EXTENSIONS } from "./constants";

import { useSnackbar } from "notistack";

const FileUpload = (props) => {
  const { onClose, open } = props;

  //Upload parameters
  const [selectedFileType, setSelectedFileType] = useState(1);
  const [allowedFileExtensions, setAllowedFileExtensions] = useState(
    REFERENCE_FILE_EXTENSIONS
  );
  const [fileExtensionsError, setFileExtensionsError] = useState(false);

  //File upload progress
  const [file, setFile] = useState("");
  const [fileName, setFileName] = useState("");
  const [loading, setLoading] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const handleClose = (doRefresh, fileTypeId) => {
    onClose(doRefresh, fileTypeId);
    setSelectedFileType(1);
    setAllowedFileExtensions(REFERENCE_FILE_EXTENSIONS);
    setFile("");
    setFileName("");
    setFileExtensionsError(false);
  };

  const handleFileTypeChange = (event) => {
    const fileTypeId = event.target.value;
    const extensions = getAllowedFileExtensions(fileTypeId);

    setFile("");
    setSelectedFileType(fileTypeId);
    setAllowedFileExtensions(extensions);
  };

  const handleFileUpload = (event) => {
    const file = event.target.files[0];
    const fileDetails = getFileDetails(file.name);
    const isFileExtensionAllowed = allowedFileExtensions.includes(
      fileDetails.extension
    );

    setFile(file);
    setFileName(fileDetails.name);
    setFileExtensionsError(!isFileExtensionAllowed);
  };

  const upload = () => {
    setLoading(true);

    const formData = new FormData();
    let endopointCall = "";
    formData.append("file", file);

    const headers = {
      "Content-Type": "multipart/form-data",
    };

    const fileType = FILE_TYPES.find(
      (fileType) => fileType.id === selectedFileType
    );

    const fileInfo = {
      name: fileName,
      fileType: fileType.name.toLowerCase(),
    };

    formData.append(
      "fileInfo",
      new Blob([JSON.stringify(fileInfo)], { type: "application/json" })
    );

    if (selectedFileType === FileType.REFERENCE_GENOME) {
      endopointCall = axiosPrivate.post("/reference/upload", formData, {
        headers: headers,
      });
    } else {
      endopointCall = axiosPrivate.post("/file/upload", formData, {
        headers: headers,
      });
    }

    endopointCall
      .then(() => {
        enqueueSnackbar("File uploaded!");
        handleClose(true, selectedFileType);
      })
      .finally(() => setLoading(false));
  };

  return (
    <Dialog
      onClose={() => handleClose(false, "")}
      open={open}
      size="sm"
      fullWidth
    >
      <DialogTitle>File upload</DialogTitle>
      <DialogContent>
        <Box p={1}>
          <Grid container spacing={1}>
            <Grid item xs={6}>
              <FormControl fullWidth>
                <InputLabel id="file-type">File type</InputLabel>
                <Select
                  labelId="file-type"
                  id="file-type-select"
                  value={selectedFileType}
                  label="File type"
                  onChange={handleFileTypeChange}
                >
                  {FILE_TYPES.map((fileType) => (
                    <MenuItem key={fileType.id} value={fileType.id}>
                      {fileType.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Box>
        <Box p={1}>
          <TextField
            fullWidth
            type="file"
            onChange={handleFileUpload}
            error={fileExtensionsError}
            helperText={`Allowed extensions: ${allowedFileExtensions.join(
              ", "
            )}`}
          />
        </Box>
        <Box p={1}>
          <TextField
            fullWidth
            id="fileName"
            label="File name"
            value={fileName}
            onChange={(event) => {
              setFileName(event.target.value);
            }}
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <LoadingButton
          disabled={!(file && fileName) || fileExtensionsError}
          loading={loading}
          variant="contained"
          onClick={upload}
        >
          Upload file
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
};

export default FileUpload;
