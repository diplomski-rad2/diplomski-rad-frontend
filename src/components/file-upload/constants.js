import { ALIGNMENT, ANNOTATION, VARIANT } from "../../services/constants";

export const FILE_TYPES = [
    { id: 1, name: 'Reference genome' },
    { id: 2, name: ALIGNMENT.charAt(0).toUpperCase() + ALIGNMENT.slice(1) },
    { id: 3, name: ANNOTATION.charAt(0).toUpperCase() + ANNOTATION.slice(1) },
    { id: 4, name: VARIANT.charAt(0).toUpperCase() + VARIANT.slice(1) }
];

export const FILE_SUBTYPES = [ALIGNMENT, ANNOTATION, VARIANT];

export const REFERENCE_FILE_EXTENSIONS = ['fasta', 'fa', 'fna'];
export const ALIGNMENT_FILE_EXTENSIONS = ['bam', 'cram'];
export const ANNOTATION_FILE_EXTENSIONS = ['bed', 'gff', 'gff3', 'gtf', 'bedpe', 'gz'];
export const VARIANT_FILE_EXTENSIONS = ['vcf', 'gz'];