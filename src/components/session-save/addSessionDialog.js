import { useState } from "react";

import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@mui/material";

import { axiosPrivate } from "../../services/axios";

import { getSession } from "./utils";

import { useSnackbar } from "notistack";

const AddSessionDialog = (props) => {
  const { onClose, open } = props;

  const [name, setName] = useState("");

  const { enqueueSnackbar } = useSnackbar();

  const onClickSaveSession = () => {
    axiosPrivate
      .post("/session", {
        id: null,
        name: name,
        data: JSON.stringify(getSession()),
      })
      .then(() => {
        enqueueSnackbar(`Session ${name} saved!`);
        handleClose(true);
      });
  };

  const handleClose = (doRefresh) => {
    onClose(doRefresh);
    setName("");
  };

  return (
    <Dialog fullWidth size="sm" open={open} onClose={() => handleClose(false)}>
      <DialogTitle>{"Add new session"}</DialogTitle>
      <DialogContent>
        <Box p={1}>
          <TextField
            id="session"
            fullWidth
            label="Session name"
            value={name}
            onChange={(event) => {
              setName(event.target.value);
            }}
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button variant="contained" onClick={() => handleClose(false)}>
          Close
        </Button>
        <Button
          variant="contained"
          disabled={name === ""}
          onClick={onClickSaveSession}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddSessionDialog;
