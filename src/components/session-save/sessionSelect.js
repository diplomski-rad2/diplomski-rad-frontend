import { useState } from "react";

import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

import { loadSession } from "./utils";

const SessionSelect = (props) => {
  const { sessions, disabled } = props;

  const [selectedSession, setSelectedSession] = useState("");

  const handleSessionChange = (event) => {
    const session = event.target.value;

    setSelectedSession(session);
    loadSession(session);
  };

  return (
    <FormControl fullWidth>
      <InputLabel id="sessions-label">Sessions</InputLabel>
      <Select
        sx={{ textAlign: "left" }}
        labelId="sessions-label"
        id="sessions"
        value={selectedSession}
        label="Sessions"
        onChange={handleSessionChange}
        disabled={disabled}
      >
        {sessions.map((session) => (
          <MenuItem key={session.data} value={session.data}>
            {session.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default SessionSelect;
