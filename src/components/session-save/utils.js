import igv from 'igv/dist/igv';

export const getSession = () => {
    return igv.browser.toJSON();
}

export const loadSession = (sessionData) => {
    igv.browser.loadSessionObject(JSON.parse(sessionData));
}