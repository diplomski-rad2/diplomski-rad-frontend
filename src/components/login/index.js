import { Link, useLocation, useNavigate } from "react-router-dom";

import { Box, Button, Grid, TextField, Typography } from "@mui/material";

import axios from "../../services/axios";

import useAuth from "../../hooks/useAuth";

import { useFormik } from "formik";
import { enqueueSnackbar } from "notistack";
import * as yup from "yup";

const Login = () => {
  const { setAuth } = useAuth();

  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || "/";

  const validation = yup.object({
    email: yup
      .string()
      .email("Email address is not valid")
      .required("Email is required"),
    password: yup.string().required("Password is required"),
  });

  const handleSubmit = (values) => {
    axios
      .post("/auth/authenticate", values)
      .then((res) => {
        enqueueSnackbar("Login successful");

        const email = values.email;
        const accessToken = res.data.token;

        setAuth({ email, accessToken });
        navigate(from, { replace: true });

        formik.resetForm();
      })
      .catch((error) => {
        console.error(error);
        enqueueSnackbar("Login failed");
      });
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validation,
    onSubmit: (values) => {
      handleSubmit(values);
    },
    enableReinitialize: true,
  });

  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      height="100vh"
      component="form"
      onSubmit={formik.handleSubmit}
      onReset={formik.handleReset}
    >
      <Box width={400}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h3" align="center">
              Login
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              id="email"
              name="email"
              label="Email"
              variant="outlined"
              type="email"
              value={formik.values.email}
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              id="password"
              name="password"
              label="Password"
              variant="outlined"
              type="password"
              value={formik.values.password}
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
            />
          </Grid>
          <Grid item xs={6}>
            <Button fullWidth variant="contained" type="submit">
              Login
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button fullWidth variant="contained">
              <Link
                style={{ textDecoration: "none", color: "white" }}
                to="/register"
              >
                Create an account
              </Link>
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default Login;
