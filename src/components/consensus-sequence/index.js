import { useState } from "react";

import {
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  TextField,
  Typography,
} from "@mui/material";

import { axiosPrivate } from "../../services/axios";

import { useSnackbar } from "notistack";

const ConsensusSequence = (props) => {
  const { onClose, open } = props;
  const { referenceGenomeId, referenceGenomeName } = props.referenceGenome;
  const variantFiles = props.variantFiles;

  const [selectedVariantId, setSelectedVariantFileId] = useState("");
  const [fileName, setFileName] = useState("");
  const [quality, setQuality] = useState(0);
  const [depth, setDepth] = useState(0);
  const { enqueueSnackbar } = useSnackbar();

  const handleClose = (doRefresh) => {
    onClose(doRefresh);
    setFileName("");
    setQuality(0);
    setDepth(0);
  };

  const handleVariantFileChange = (event) => {
    const variantFileId = event.target.value;
    setSelectedVariantFileId(variantFileId);
  };

  const generate = () => {
    axiosPrivate
      .post("/processing/consensus-sequence", {
        fileName: fileName,
        referenceGenomeId: referenceGenomeId,
        variantFileId: selectedVariantId,
        minQuality: quality,
        minDepth: depth,
      })
      .then(() => {
        enqueueSnackbar("Consensus sequence generated!");
        handleClose(true);
      });
  };

  return (
    <Dialog
      onClose={() => handleClose(false, "")}
      open={open}
      size="sm"
      fullWidth
    >
      <DialogTitle>Generate consensus sequence</DialogTitle>
      <DialogContent>
        <Stack direction="column" spacing={2}>
          <Typography variant="h6" component="h6">
            Reference genome:{" "}
            <Chip label={referenceGenomeName} variant="outlined" />
          </Typography>
          <FormControl fullWidth>
            <InputLabel id="variant-file-select-label">Variant file</InputLabel>
            <Select
              sx={{ textAlign: "left" }}
              labelId="variant-file-select-label"
              id="variant-file-select"
              value={selectedVariantId}
              label="Variant file"
              onChange={handleVariantFileChange}
            >
              {variantFiles.map((variantFile) => (
                <MenuItem
                  key={variantFile.id}
                  value={variantFile.id}
                  name={variantFile.name}
                >
                  {variantFile.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <TextField
            id="fileName"
            label="File name"
            value={fileName}
            onChange={(event) => setFileName(event.target.value)}
            sx={{ mt: 1 }}
          />
          <Stack direction="row" spacing={2}>
            <TextField
              fullWidth
              id="minimal-quality"
              label="Minimal quality"
              type="number"
              value={quality}
              onChange={(event) => {
                setQuality(event.target.value);
              }}
            />
            <TextField
              fullWidth
              id="minimal-depth"
              label="Minimal depth"
              type="number"
              value={depth}
              onChange={(event) => {
                setDepth(event.target.value);
              }}
            />
          </Stack>
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button
          disabled={fileName === ""}
          variant="contained"
          onClick={generate}
        >
          Generate
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConsensusSequence;
