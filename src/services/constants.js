export const FileType = {
    REFERENCE_GENOME: 1,
    FILE: 2,
};

export const ALIGNMENT = 'alignment';
export const VARIANT = 'variant';
export const ANNOTATION = 'annotation';

export const CONTENT_DISPOSITION = 'content-disposition';
export const BASE_URL = "http://localhost:8080";
