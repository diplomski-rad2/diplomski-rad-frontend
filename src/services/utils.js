import igv from "igv/dist/igv";

import { axiosPrivate } from "./axios";

import {
  ALIGNMENT,
  ANNOTATION,
  BASE_URL,
  CONTENT_DISPOSITION,
  VARIANT,
} from "./constants";

import { saveAs } from "file-saver";

export const loadAlignmentTrack = (name, alignmentFileId, token) => {
  igv.browser.loadTrack({
    type: ALIGNMENT,
    format: "bam",
    name: name,
    url: `${BASE_URL}/file/${alignmentFileId}`,
    indexURL: `${BASE_URL}/file/index/${alignmentFileId}`,
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const loadVariantTrack = (name, variantFileId, token) => {
  igv.browser.loadTrack({
    type: VARIANT,
    format: "vcf",
    url: `${BASE_URL}/file/${variantFileId}`,
    indexURL: `${BASE_URL}/file/index/${variantFileId}`,
    headers: { Authorization: `Bearer ${token}` },
    name: name,
    expandedCallHeight: 4,
    visibilityWindow: 1000,
    height: 50,
  });
};

export const loadAnnotationTrack = (name, annotationFileId, token) => {
  igv.browser.loadTrack({
    name: name,
    type: ANNOTATION,
    format: "gff3",
    displayMode: "expanded",
    height: 200,
    url: `${BASE_URL}/file/${annotationFileId}`,
    headers: { Authorization: `Bearer ${token}` },
    visibilityWindow: 1000000,
    colorBy: "biotype",
  });
};

export const getAllTracks = () => {
  const tracks = igv.browser?.findTracks().slice(3);
  return tracks;
};

export const clearAllTracks = () => {
  const tracks = igv.browser?.trackViews;

  if (!tracks) {
    return;
  }

  if (igv.browser) {
    return;
  }

  tracks.forEach((view) => igv.browser.removeTrack(view.track));
};

export const getVariantFileName = () => {
  return igv.browser.findTracks("type", "variant")[0].name;
};

export const getFileName = (contentDisposition) => {
  let fileName = "fileName";

  const match = contentDisposition.match(/filename="(.+)"/);
  if (match && match.length > 1) {
    fileName = match[1];
  }

  return fileName;
};

export const referenceGenomeDownload = (referenceGenomeId) => {
  axiosPrivate
    .get(`/reference/download/${referenceGenomeId}`, { responseType: "blob" })
    .then((response) => {
      const contentDisposition = response.headers[CONTENT_DISPOSITION];

      if (!contentDisposition) {
        console.error("Content disposition not found");
      }

      saveAs(response.data, getFileName(contentDisposition));
    })
    .catch((error) => {
      console.error("Error downloading the file:", error);
    });
};

export const fileDownload = (fileId) => {
  axiosPrivate
    .get(`/file/download/${fileId}`, { responseType: "blob" })
    .then((response) => {
      const contentDisposition = response.headers[CONTENT_DISPOSITION];

      if (!contentDisposition) {
        console.error("Content disposition not found");
      }

      saveAs(response.data, getFileName(contentDisposition));
    })
    .catch((error) => {
      console.error("Error downloading the file:", error);
    });
};
