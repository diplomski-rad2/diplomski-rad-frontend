import { useEffect } from "react";

import { axiosPrivate } from "../services/axios";
import useAuth from "./useAuth";

const useAxiosPrivate = () => {
  const { auth } = useAuth();

  useEffect(() => {
    const requestIntercept = axiosPrivate.interceptors.request.use(
      (config) => {
        if (!config.headers["Authorization"]) {
          config.headers["Authorization"] = `Bearer ${auth?.accessToken}`;
        }

        return config;
      },
      (error) => Promise.reject(error)
    );

    const responseInterceptor = axiosPrivate.interceptors.response.use(
      (response) => response,
      async (error) => {
        const prevRequest = error?.config;

        // refresh token
        if (error?.response?.stats === 403 && !prevRequest?.sent) {
          prevRequest.sent = true;
          //refresh
          prevRequest.headers["Authorization"] = `Bearer token`;
          return axiosPrivate(prevRequest);
        }

        return Promise.reject(error);
      }
    );

    return () => {
      axiosPrivate.interceptors.request.eject(requestIntercept);
      axiosPrivate.interceptors.response.eject(responseInterceptor);
    };
  }, [auth]);

  return axiosPrivate;
};

export default useAxiosPrivate;
