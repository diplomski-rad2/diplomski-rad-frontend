import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Select,
} from "@mui/material";
import { useState } from "react";

import AddRoadIcon from "@mui/icons-material/AddRoad";
import DownloadIcon from "@mui/icons-material/Download";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import SaveIcon from "@mui/icons-material/Save";

import ConsensusSequence from "../components/consensus-sequence";
import FileSelect from "../components/file-select";
import FilteringQuick from "../components/filtering/filteringQuick";
import Igv from "../components/igv";
import AddSessionDialog from "../components/session-save/addSessionDialog";
import SessionSelect from "../components/session-save/sessionSelect";

import {
  clearAllTracks,
  getAllTracks,
  referenceGenomeDownload,
} from "../services/utils";

import { axiosPrivate } from "../services/axios";

const LandingPage = (props) => {
  const { referenceGenomes, files, sessions, variantFiles } = props;
  const { setReferenceGenomes, setFiles, setSessions } = props;
  const { refreshSources } = props;

  const [selectedReferenceGenomeId, setSelectedReferenceGenomeId] =
    useState("");
  const [referenceGenomeData, setReferenceGenomeData] = useState({});

  const [isSaveSessionDialog, setIsSaveSessionDialog] = useState(false);

  const [isConsensusSequenceOpenDialog, setIsConsensusSequenceOpenDialog] =
    useState(false);
  const [selectedReferenceGenome, setSelectedReferenceGenome] = useState({
    referenceGenomeId: "",
    referenceGenomeName: "",
    sequences: [],
  });

  const [isFilteringDialogOpen, setIsFilteringDialogOpen] = useState(false);
  const [allFiles, setAllFiles] = useState([]);

  const handleReferenceGenomeChange = (event) => {
    const referenceGenomeId = event.target.value;

    clearAllTracks();
    setSelectedReferenceGenomeId(referenceGenomeId);
    const referenceGenome = referenceGenomes.filter(
      (referenceGenome) => referenceGenome.id === referenceGenomeId
    )[0];
    setReferenceGenomeData({
      id: referenceGenomeId,
      name: referenceGenome.name,
      sequences: referenceGenome.sequences,
    });
  };

  const handleSessionSaveOpen = () => {
    setIsSaveSessionDialog(true);
  };

  const handleSessionSaveClose = (doRefresh) => {
    if (doRefresh) {
      axiosPrivate.get("/session").then((res) => setSessions(res.data));
    }
    setIsSaveSessionDialog(false);
  };

  const handleConsensusSequenceOpen = () => {
    const referenceGenomeName = referenceGenomes.filter(
      (referenceGenome) => referenceGenome.id === selectedReferenceGenomeId
    )[0].name;

    setSelectedReferenceGenome({
      referenceGenomeId: selectedReferenceGenomeId,
      referenceGenomeName: referenceGenomeName,
    });
    setIsConsensusSequenceOpenDialog(true);
  };

  const handleConsensusSequenceClose = (doRefresh) => {
    if (doRefresh) {
      refreshReferenceGenomesAndFiles();
    }
    setIsConsensusSequenceOpenDialog(false);
  };

  const handleFilteringOpen = () => {
    const tracks = getAllTracks();

    const sequenceTrack = referenceGenomeData;
    const loadedTracks = tracks.map(
      (track) => files.filter((file) => file.name === track.config.name)[0]
    );

    setAllFiles([sequenceTrack, ...loadedTracks]);
    setIsFilteringDialogOpen(true);
  };

  const handleFilteringClose = (doRefresh) => {
    if (doRefresh) {
      refreshReferenceGenomesAndFiles();
    }
    setIsFilteringDialogOpen(false);
  };

  const refreshReferenceGenomesAndFiles = () => {
    Promise.all([
      axiosPrivate.get("/reference"),
      axiosPrivate.get("/file"),
    ]).then(([{ data: refGenomsResponse }, { data: filesResponse }]) => {
      setReferenceGenomes(refGenomsResponse);
      setFiles(filesResponse);
      refreshSources(refGenomsResponse, filesResponse);
    });
  };

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={3}>
          <FormControl fullWidth>
            <InputLabel id="reference-genome-select-label">
              Reference genome
            </InputLabel>
            <Select
              sx={{ textAlign: "left" }}
              labelId="reference-genome-select-label"
              id="reference-genome-select"
              value={selectedReferenceGenomeId}
              label="Reference genome"
              renderValue={(selected) =>
                referenceGenomes.filter(
                  (referenceGenome) => referenceGenome.id === selected
                )[0].name
              }
              onChange={handleReferenceGenomeChange}
            >
              {referenceGenomes.map((referenceGenome) => (
                <MenuItem
                  key={referenceGenome.id}
                  value={referenceGenome.id}
                  name={referenceGenome.name}
                >
                  <ListItemText primary={referenceGenome.name} />
                  <ListItemIcon
                    onClick={(event) => {
                      event.stopPropagation();
                      referenceGenomeDownload(referenceGenome.id);
                    }}
                  >
                    <DownloadIcon />
                  </ListItemIcon>
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={3}>
          <FileSelect
            files={files}
            disabled={selectedReferenceGenomeId === ""}
          />
        </Grid>
        <Grid item xs={3}>
          <SessionSelect
            sessions={sessions}
            disabled={selectedReferenceGenomeId === ""}
          />
        </Grid>
        <Grid item xs={1}>
          <Button
            fullWidth
            size="small"
            variant="contained"
            startIcon={<SaveIcon />}
            onClick={handleSessionSaveOpen}
            disabled={selectedReferenceGenomeId === ""}
          >
            Save session
          </Button>
        </Grid>
        <Grid item xs={1}>
          <Button
            fullWidth
            size="small"
            variant="contained"
            startIcon={<AddRoadIcon />}
            onClick={handleConsensusSequenceOpen}
            disabled={selectedReferenceGenomeId === ""}
          >
            Generate consensus
          </Button>
        </Grid>
        <Grid item xs={1}>
          <Button
            fullWidth
            size="small"
            variant="contained"
            startIcon={<FilterAltIcon />}
            onClick={handleFilteringOpen}
            disabled={selectedReferenceGenomeId === ""}
          >
            Quick filtering
          </Button>
        </Grid>
        <Grid item xs={12}>
          {selectedReferenceGenomeId && (
            <Igv referenceGenomeData={referenceGenomeData} />
          )}
        </Grid>
      </Grid>
      <AddSessionDialog
        open={isSaveSessionDialog}
        onClose={handleSessionSaveClose}
      />
      <ConsensusSequence
        open={isConsensusSequenceOpenDialog}
        onClose={handleConsensusSequenceClose}
        referenceGenome={selectedReferenceGenome}
        variantFiles={variantFiles}
      />
      <FilteringQuick
        open={isFilteringDialogOpen}
        onClose={handleFilteringClose}
        files={allFiles}
      />
    </>
  );
};

export default LandingPage;
